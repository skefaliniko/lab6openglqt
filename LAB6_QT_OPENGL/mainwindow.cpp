#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "glwidget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_radioButton_clicked(bool checked) { //simple, =1
    if (checked) {
        this->ui->GLwidget->choice = 1;
    }
}

void MainWindow::on_radioButton_2_clicked(bool checked) { //sphere, =2
    if (checked) {
        this->ui->GLwidget->choice = 2;
    }
}

void MainWindow::on_radioButton_3_clicked(bool checked) { //thorus, =3
    if (checked) {
        this->ui->GLwidget->choice = 3;
    }
}
