#include "glwidget.h"
#include <GL/glut.h>
#include "math.h"

GLWidget::GLWidget(QWidget *parent) : QGLWidget(parent) {
    this->x = 0.0;
    this->y = 0.0;
    this->z = 0.0;
    this->choice = 2; //2  -  default value
    this->ang = 0.25;
    connect( &timer, SIGNAL(timeout()), this, SLOT(updateGL()) );
    timer.start(12);
}

void GLWidget::initializeGL()
{
    glClearColor(0, 0, 0, 1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0,0,5, 0,0,0, 0,1,0);
    ang += 0.25;

    switch (choice) {
    case 1:         //simple
        glBegin(GL_LINE_LOOP);
           glVertex2f(1.0, 0.0);
           glVertex2f(0.0, 2.0);
           glColor3f(1, 1, 0);
           glVertex2f(0.5, -0.5);
           glVertex2f(1.0, 0.5);
           glVertex2f(-1.0, 0.0);
        glEnd();

//        glColor3f(0.0, 0.0, 0.0);           black
//        glColor3f(1.0, 0.0, 0.0);           red
//        glColor3f(0.0, 1.0, 0.0);           green
//        glColor3f(1.0, 1.0, 0.0);           yellow
//        glColor3f(0.0, 0.0, 1.0);           blue
//        glColor3f(1.0, 0.0, 1.0);           magenta
//        glColor3f(0.0, 1.0, 1.0);           cyan
//        glColor3f(1.0, 1.0, 1.0);           white

        glBegin(GL_QUADS);
             glColor3f(0.0, 0.0, 1.0);
             glVertex3f(-1.5f, -0.9f,  0.0f );
             glColor3f(0.0, 1.0, 1.0);
             glVertex3f(-0.5f,  1.4f,  0.0f );
             glColor3f(1.0, 0.0, 1.0);
             glVertex3f( 0.5f,  0.5f,  0.0f );
             glColor3f(1.0, 0.0, 0.0);
             glVertex3f( -1.1f, -0.5f,  0.0f );
        glEnd();

        glBegin(GL_POLYGON);
           glColor3f(0.0, 1.0, 0.0);
           glVertex2f(0.0, 0.0);
           glVertex2f(0.0, 3.0);
           glVertex2f(4.0, 3.0);
           glVertex2f(6.0, 1.5);
           glVertex2f(4.0, 0.0);
        glEnd();

        glBegin(GL_LINE_STRIP);
           glColor3f(1.0, 1.0, 1.0);
           glVertex2f(0.5, 0.0);
           glVertex2f(0.0, -3.0);
           glVertex2f(4.0, 2.0);
           glVertex2f(-3.0, 1.5);
           glVertex2f(4.0, 0.0);
        glEnd();

        break;
    case 3:         //thorus
        glRotatef(ang, 1, 1, 0);
        glTranslatef(x,y,z);
        glColor3f(1, 0, 1);
        glutWireTorus(0.70, 1.2, 38, 38);
        break;
    default:
    case 2:         //sphere
        glRotatef(ang, -1, -1, -1);
        glTranslatef(x,y,z);
        glColor3f(0, 0, 1);
        glutWireSphere(1.5, 26, 26);
        break;
    }
}

void GLWidget::resizeGL(int w, int h)
{
    glViewport(0,0, w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (float)w/h, 0.01, 100.0);

    updateGL();
}
